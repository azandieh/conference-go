# Generated by Django 4.0.3 on 2023-02-20 02:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attendees', '0002_accountvo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accountvo',
            name='email',
            field=models.EmailField(max_length=200, unique=True),
        ),
        migrations.AlterField(
            model_name='accountvo',
            name='first_name',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='accountvo',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='accountvo',
            name='last_name',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='accountvo',
            name='updated',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
